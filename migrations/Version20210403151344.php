<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210403151344 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE hotel (number INTEGER NOT NULL, area VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, category VARCHAR(255) NOT NULL, valid_from DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, report_created_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , website VARCHAR(255) NOT NULL, PRIMARY KEY(number))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE hotel');
    }
}
