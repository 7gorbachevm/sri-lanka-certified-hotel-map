<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\HotelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    public function __construct(
        private HotelRepository $hotelRepository,
    ) {
    }


    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $hotels = $this->hotelRepository->getCurrentlyListedHotels();
        return $this->render('index.html.twig', ['hotels' => $hotels]);
    }
}