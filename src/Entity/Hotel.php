<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Hotel
{
    public function __construct(
        /**
         * @ORM\Id()
         * @Orm\Column(type="integer")
         */
        public int $number,
        /** @ORM\Column(type="string") */
        public string $area,
        /** @ORM\Column(type="string") */
        public string $name,
        /** @ORM\Column(type="string") */
        public string $category,
        /** @ORM\Column(type="datetime_immutable") */
        public \DateTimeImmutable $validFrom,
        /** @ORM\Column(type="float") */
        public float $latitude,
        /** @ORM\Column(type="float") */
        public float $longitude,
        /** @ORM\Column(type="datetime_immutable") */
        public \DateTimeImmutable $reportCreatedAt,
        /** @ORM\Column(type="string") */
        public string|null $website = null,
    ) {
    }
}