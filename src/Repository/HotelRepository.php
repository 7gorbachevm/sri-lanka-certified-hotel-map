<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Hotel;
use Doctrine\ORM\EntityManagerInterface;

class HotelRepository
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * @param Hotel[] $hotels
     */
    public function save(array $hotels): void
    {
        $this->entityManager->transactional(function () use ($hotels) {
            $this->entityManager->getConnection()->executeQuery('DELETE FROM hotel');
            foreach ($hotels as $hotel) {
                $this->entityManager->persist($hotel);
            }
            $this->entityManager->flush();
        });
    }

    public function getCurrentlyListedHotels(): array
    {
        return $this
            ->entityManager
            ->getRepository(Hotel::class)
            ->findAll();
    }
}