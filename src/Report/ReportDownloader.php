<?php

declare(strict_types=1);

namespace App\Report;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ReportDownloader
{
    public function __construct(
        private HttpClientInterface $httpClient,
        private LoggerInterface $logger,
    ) {
    }

    public function download(): File
    {
        $url = 'https://srilanka.travel/helloagain/documents/Level1Hotels/Level1HotelList.pdf';
        $response = $this->httpClient->request('GET', $url, [
            'buffer' => false,
        ]);

        if ($response->getStatusCode() !== 200) {
            throw new \Exception(sprintf("Unable to download the PDF report. Status %s Response: %s", $response->getStatusCode(), $response->getContent()));
        }

        $this->logger->info('PDF download has started');
        $filePath = sys_get_temp_dir() . '/Level1HotelList.pdf';
        $fileHandler = fopen($filePath, 'w');
        foreach ($this->httpClient->stream($response) as $chunk) {
            fwrite($fileHandler, $chunk->getContent());
        }
        $this->logger->info(sprintf("PDF download completed. File saved as %s", $filePath));

        return new File($filePath);
    }
}