<?php

declare(strict_types=1);

namespace App\Report;

use App\Entity\Hotel;
use App\Geocoder\GeocoderInterface;
use Psr\Log\LoggerInterface;
use Smalot\PdfParser\Parser;

class ReportParser
{
    public function __construct(
        private Parser $pdfParser,
        private GeocoderInterface $geocoder,
        private LoggerInterface $logger,
    ) {
    }

    /**
     * @param string $pdfPath
     * @return Hotel[]
     * @throws \Exception
     */
    public function parse(string $pdfPath): array
    {
        $pdf = $this->pdfParser->parseFile($pdfPath);
        $this->logger->info('Parsing PDF started');
        $text = $pdf->getText();
        $rows = explode(separator: "\n", string: $text);
        $startsWithNumber = fn(string $row) => preg_match('/^\d/', $row) === 1;
        $hotelRows = array_filter($rows, $startsWithNumber);

        $reportCreatedAt = (new \DateTimeImmutable($pdf->getDetails()['CreationDate']))
            ->setTimezone(new \DateTimeZone('UTC'));

        $this->logger->info('Geocoding started');
        $hotels = array_map(function ($row, $hotelIndex) use ($reportCreatedAt, $hotelRows) {
            [$number, $area, $title, $category, $validFrom, $website] = explode(separator: "\t", string: $row);

            // Fix various typos in PDF document :(
            $validFrom = str_replace(search: 'rth', replace: 'th', subject: $validFrom);
            $area = $this->fixAreaSpelling($area);

            $location = $this->geocoder->geocode(sprintf('%s sri lanka', $area));
            $this->logger->info(sprintf('Geocoding: Hotel #%s (%d out of %d)', $number, $hotelIndex, count($hotelRows)));

            return new Hotel(
                number: (int) $number,
                area: $area,
                name: $title,
                category: $category,
                validFrom: new \DateTimeImmutable($validFrom),
                latitude: $location->getLatitude(),
                longitude: $location->getLongitude(),
                reportCreatedAt: $reportCreatedAt,
                website: $website ?? null
            );
        }, $hotelRows, array_keys(array_values($hotelRows)));

        $this->logger->info('Report has been parsed');

        return $hotels;
    }

    private function fixAreaSpelling(string $area): string
    {
        $dictionary = [
            'Negambo' => 'Negombo',
            'Kaluthara' => 'Kalutara',
            'Benthota' => 'Bentota',
            'Nuweraeliya' => 'Nuwara Eliya',
            'Arugambay' => 'Arugam Bay',
            'Irakkakandy' => 'Nilaveli',
            'Theldeniya' => 'Teldeniya',
            'Sooriyakanda' => 'Suriyakanda',
            'Kudawaskaduwa' => 'Kuda Waskaduwa'
        ];

        foreach ($dictionary as $wrongSpelling => $correctSpelling) {
            $area = str_contains(haystack: $area, needle: $wrongSpelling)
                ? str_replace(search: $wrongSpelling, replace: $correctSpelling, subject: $area)
                : $area;
        }

        return $area;
    }
}