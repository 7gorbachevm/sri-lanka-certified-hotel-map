<?php

declare(strict_types=1);

namespace App\Report;

use App\Repository\HotelRepository;
use Psr\Log\LoggerInterface;

class ReportSaver
{
    public function __construct(
        private ReportDownloader $reportDownloader,
        private ReportParser $reportParser,
        private HotelRepository $reportRepository,
        private LoggerInterface $logger,
    ) {
    }

    public function save(): void
    {
        $file = $this->reportDownloader->download();
        $hotels = $this->reportParser->parse($file->getPath());
        $this->reportRepository->save($hotels);
        $this->logger->info('Report has been saved');
    }
}