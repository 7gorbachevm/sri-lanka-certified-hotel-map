<?php

declare(strict_types=1);

namespace App\Report;

class File
{
    public function __construct(
        private string $path
    ) {
        if (!file_exists($path)) {
            throw new \Exception("File at path {$path} does not exist");
        }
    }

    public function getPath(): string
    {
        return $this->path;
    }
}