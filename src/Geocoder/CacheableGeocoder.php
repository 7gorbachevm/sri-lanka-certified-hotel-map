<?php

declare(strict_types=1);

namespace App\Geocoder;

use Symfony\Contracts\Cache\CacheInterface;

class CacheableGeocoder implements GeocoderInterface
{
    public function __construct(
        private Geocoder $geocoder,
        private CacheInterface $cache
    ) {
    }

    public function geocode(string $address): Location
    {
        return $this->cache->get($address, function () use ($address) {
            return $this->geocoder->geocode($address);
        });
    }
}