<?php

declare(strict_types=1);

namespace App\Geocoder;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class Geocoder implements GeocoderInterface
{
    public function __construct(
        private HttpClientInterface $httpClient,
    ) {
    }

    public function geocode(string $address): Location
    {
        $url = sprintf('http://api.geonames.org/searchJSON?q=%s&maxRows=2&style=full&username=Egor', urlencode($address));
        $response = $this->httpClient->request('GET', $url);
        if ($response->getStatusCode() !== 200) {
            throw new \Exception(sprintf('Unable to geocode address: %s', $response->getContent()));
        }
        $result = json_decode($response->getContent(), true);

        return new Location(
            latitude: (float)$result['geonames'][0]['lat'],
            longitude: (float)$result['geonames'][0]['lng']
        );
    }
}