<?php

declare(strict_types=1);

namespace App\Geocoder;

interface GeocoderInterface
{
    public function geocode(string $address): Location;
}