<?php

namespace App\Command;

use App\Report\ReportSaver;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseReportCommand extends Command
{
    protected static $defaultName = 'app:parse-report';

    public function __construct(private ReportSaver $reportSaver)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->reportSaver->save();
        return Command::SUCCESS;
    }
}