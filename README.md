# Sri-Lanka Level 1 Certified hotel map

Web app that displays Sri-Lank Level 1 certified hotel on map.

## CLI
- `bin/console app:parse-report -vvv`

## How does it work

- Once a day the script downloads PDF report with Level 1 certified hotels, parses PDF, geocodes the position of each hotel and saves it to the database.
- HTML page is displaying the map of the hotels using Google Maps API